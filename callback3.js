/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is 
    passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs= require("fs");
const path=require('path');


function cardInformation(id,cb){
    setTimeout(()=>{
        fs.readFile(path.resolve(__dirname,'./cards.json'),'utf8',(err,data)=>{
            if(err){
                console.error(err);
            }else{
                data=JSON.parse(data);
                let result=data[id];
                if(result){
                    cb(null,result);
                    console.log('back to where data is passed');
                }
                else{
                    cb(new Error('card Not found'),result);
                }
            }
        })
    },2000)
}
module.exports=cardInformation;
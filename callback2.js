/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID 
    that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs= require("fs");
const path=require('path');


function listInformation(id,cb){
    setTimeout(()=>{

        fs.readFile(path.resolve(__dirname,'./lists.json'),'utf8',(err,data)=>{
            if(err){
                console.error(err);
            }else{
                data=JSON.parse(data);
                let result=data[id];
                if(result){
                    cb(null,result);
                    console.log('back to where data is passed');
                }else{
                    cb(new Error('List not found'),result)
                }
            }
        })
    },2000)
}
module.exports=listInformation;
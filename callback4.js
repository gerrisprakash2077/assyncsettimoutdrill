/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not 
    need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boardInformation=require('./callback1');
const listInformation=require('./callback2');
const cardInformation=require('./callback3');
function thanosBoardMindInfo(cb){
    boardInformation('mcu453ed',(err,boardData)=>{
        if(err){
            console.error(err);
        }else{
            if(boardData){
                cb(null,boardData);
                listInformation('mcu453ed',(err,listData)=>{
                    if(err){
                        console.error(err);
                    }else{
                        if(listData){
                            cb(null,listData)
                            cardInformation("qwsa221",(err,cardData)=>{
                                if(err){
                                    console.error(err);
                                }else{
                                    if(cardData){
                                        cb(null,cardData)
                                    }else{
                                        cb(new Error("card not found"))
                                    }
                                }
                            })
                        }
                        else{
                            cb(new Error('list not found'))
                        }
                    }
                })
            }else{
                cb(new Error('board not Found'))
            }
        }
    })    
}
module.exports=thanosBoardMindInfo;

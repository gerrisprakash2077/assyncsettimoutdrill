/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not 
    need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const thanosBoardMindInfo=require('../callback4');
thanosBoardMindInfo((err,data)=>{
    if(err){
        console.error(err);
    }else{
        console.log(data);
    }
});
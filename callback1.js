/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given 
    list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/


function boardInformation(id,cb){
    const fs=require('fs');
    const path=require('path');
    
    setTimeout(()=>{
        fs.readFile(path.resolve(__dirname,'./boards.json'),(err,data)=>{
            if(err){
                console.error(err);
            }else{
                let result=JSON.parse(data).find((elem)=>{
                    return elem.id==id;
                });
                if(result){
                    cb(null,result);
                    console.log('back to where data is passed');
                }else{
                    cb(new Error('board not found'),result);
                }
            }
        })

    },2000);

}



module.exports=boardInformation;
//console.log(boardInformation("mcu453ed"));















